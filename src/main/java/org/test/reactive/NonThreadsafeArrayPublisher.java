package org.test.reactive;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public class NonThreadsafeArrayPublisher<T> implements Publisher<T> {

    private final T[] array;

    public NonThreadsafeArrayPublisher(T[] array) {
        this.array = array;
    }

    @Override
    public void subscribe(Subscriber<? super T> subscriber) {

        subscriber.onSubscribe(new Subscription() {

            private int next = 0;
            private long requested = 0;
            private boolean completed = false;

            @Override
            public void request(long n) {
                if (n <= 0) subscriber.onError(new IllegalArgumentException());

                boolean recursive = requested > 0;

                requested += n;

                if (recursive) return;

                int sent = 0;

                for (; sent < requested && next < array.length; sent++, next++) {
                    if (completed) return;

                    final T element = array[next];

                    if (element == null) {
                        subscriber.onError(new NullPointerException());
                        return;
                    }

                    subscriber.onNext(element);
                }

                if (completed) return;

                if (next == array.length) {
                    completed = true;
                    subscriber.onComplete();
                }

                requested = 0;
            }

            @Override
            public void cancel() {
                completed = true;
            }
        });
    }
}
