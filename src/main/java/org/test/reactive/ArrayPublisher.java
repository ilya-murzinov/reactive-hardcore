package org.test.reactive;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

public class ArrayPublisher<T> implements Publisher<T> {

    private final T[] array;
    private final IllegalArgumentException e = new IllegalArgumentException();
    private final NullPointerException npe = new NullPointerException();

    public ArrayPublisher(T[] array) {
        this.array = array;
    }

    @Override
    public void subscribe(Subscriber<? super T> subscriber) {

        subscriber.onSubscribe(new Subscription() {

            private AtomicInteger next = new AtomicInteger(0);
            private ThreadLocal<Long> requested = ThreadLocal.withInitial(() -> 0L);
            private volatile boolean completed = false;

            @Override
            public void request(long n) {
                if (n <= 0) subscriber.onError(e);

                boolean recursive = requested.get() > 0;

                requested.set(requested.get() + n);

                if (recursive) return;

                int sent = 0;

                int i = next.getAndAdd(requested.get().intValue());

                for (int index = i; index < i + requested.get() && index < array.length; index++, sent++) {
                    if (completed) return;

                    final T element = array[index];

                    if (element == null) {
                        subscriber.onError(npe);
                        return;
                    }

                    subscriber.onNext(element);
                }

                if (completed) return;

                if (i + requested.get() >= array.length && sent < requested.get()) {
                    subscriber.onComplete();
                }

                requested.set(requested.get() - sent);
            }

            @Override
            public void cancel() {
                completed = true;
            }
        });
    }

    public final <U> Publisher<U> map(Function<T, U> mapper) {
        return new MapPublisher<>(this, mapper);
    }
}
