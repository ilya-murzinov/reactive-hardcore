package org.test.reactive;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.function.Function;

public class MapPublisher<T, U> implements Publisher<U> {

    private final Publisher<T> downstream;
    private final Function<T, U> mapper;

    MapPublisher(Publisher<T> downstream, Function<T, U> mapper) {
        this.downstream = downstream;
        this.mapper = mapper;
    }

    @Override
    public void subscribe(Subscriber<? super U> subscriber) {
        downstream.subscribe(new Subscriber<T>() {

            @Override
            public void onSubscribe(Subscription s) {
                subscriber.onSubscribe(s);
            }

            @Override
            public void onNext(T t) {
                subscriber.onNext(mapper.apply(t));
            }

            @Override
            public void onError(Throwable t) {
                subscriber.onError(t);
            }

            @Override
            public void onComplete() {
                subscriber.onComplete();
            }
        });
    }
}
