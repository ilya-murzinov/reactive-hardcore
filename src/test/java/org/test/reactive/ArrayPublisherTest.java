package org.test.reactive;

import com.google.common.collect.Sets;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.reactivestreams.tck.PublisherVerification;
import org.reactivestreams.tck.TestEnvironment;
import org.testng.Assert;
import org.testng.annotations.Test;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.LongStream;

import static java.util.Arrays.asList;
import static java.util.Collections.synchronizedList;
import static java.util.concurrent.ForkJoinPool.commonPool;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.testng.Assert.assertEquals;

public class ArrayPublisherTest extends PublisherVerification<Long> {

    public ArrayPublisherTest() {
        super(new TestEnvironment());
    }

    @Override
    public Publisher<Long> createPublisher(long elements) {
        return new ArrayPublisher<>(generate(elements));
    }

    @Override
    public Publisher<Long> createFailedPublisher() {
        return null;
    }

    @Test
    public void signalsShouldBeEmittedInTheRightOrder() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        ArrayList<Long> collected = new ArrayList<>();
        ArrayList<Integer> order = new ArrayList<>();
        long toRequest = 5L;
        Long[] array = generate(toRequest);
        ArrayPublisher<Long> publisher = new ArrayPublisher<>(array);

        publisher.subscribe(new Subscriber<Long>() {
            @Override
            public void onSubscribe(Subscription s) {
                order.add(0);
                s.request(toRequest + 1);
            }

            @Override
            public void onNext(Long aLong) {
                collected.add(aLong);

                if (!order.contains(1)) {
                    order.add(1);
                }
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {
                order.add(2);
                latch.countDown();
            }
        });

        latch.await(1, TimeUnit.SECONDS);

        Assert.assertEquals(order, Arrays.asList(0, 1, 2));
        Assert.assertEquals(collected, Arrays.asList(array));
    }

    @Test
    public void mustSupportBackpressureControl() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        ArrayList<Long> collected = new ArrayList<>();
        long toRequest = 5L;
        Long[] array = generate(toRequest);
        ArrayPublisher<Long> publisher = new ArrayPublisher<>(array);
        Subscription[] subscription = new Subscription[1];

        publisher.subscribe(new Subscriber<Long>() {
            @Override
            public void onSubscribe(Subscription s) {
                subscription[0] = s;
            }

            @Override
            public void onNext(Long aLong) {
                collected.add(aLong);
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {
                latch.countDown();
            }
        });


        assertEquals(collected, Collections.emptyList());

        subscription[0].request(1);
        assertEquals(collected, asList(0L));

        subscription[0].request(1);
        assertEquals(collected, asList(0L, 1L));

        subscription[0].request(2);
        assertEquals(collected, asList(0L, 1L, 2L, 3L));

        subscription[0].request(20);

        latch.await(1, SECONDS);

        assertEquals(collected, asList(array));
    }

    @Test
    public void mustSendNPENormally() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        Long[] array = new Long[] { null };
        AtomicReference<Throwable> error = new AtomicReference<>();
        ArrayPublisher<Long> publisher = new ArrayPublisher<>(array);

        publisher.subscribe(new Subscriber<Long>() {
            @Override
            public void onSubscribe(Subscription s) {
                s.request(4);
            }

            @Override
            public void onNext(Long aLong) {
            }

            @Override
            public void onError(Throwable t) {
                error.set(t);
                latch.countDown();
            }

            @Override
            public void onComplete() {
            }
        });

        latch.await(1, SECONDS);

        Assert.assertTrue(error.get() instanceof NullPointerException);
    }

    @Test
    public void shouldNotDieInStackOverflow() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        ArrayList<Long> collected = new ArrayList<>();
        long toRequest = 1000L;
        Long[] array = generate(toRequest);
        ArrayPublisher<Long> publisher = new ArrayPublisher<>(array);

        publisher.subscribe(new Subscriber<Long>() {
            Subscription s;

            @Override
            public void onSubscribe(Subscription s) {
                this.s = s;
                s.request(1);
            }

            @Override
            public void onNext(Long aLong) {
                collected.add(aLong);

                s.request(1);
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {
                latch.countDown();
            }
        });

        latch.await(5, SECONDS);

        assertEquals(collected, asList(array));
    }

    @Test
    public void shouldBePossibleToCancelSubscription() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        ArrayList<Long> collected = new ArrayList<>();
        long toRequest = 1000L;
        Long[] array = generate(toRequest);
        ArrayPublisher<Long> publisher = new ArrayPublisher<>(array);

        publisher.subscribe(new Subscriber<Long>() {

            @Override
            public void onSubscribe(Subscription s) {
                s.cancel();
                s.request(toRequest);
            }

            @Override
            public void onNext(Long aLong) {
                collected.add(aLong);
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {
                latch.countDown();
            }
        });

        latch.await(1, SECONDS);

        assertEquals(collected, Collections.emptyList());
    }

    @Test
    public void multithreadingTest() throws InterruptedException {
        //while (true) {
            CountDownLatch latch = new CountDownLatch(1);
            ArrayList<Long> collected1 = new ArrayList<>();
            final List<Long> collected = synchronizedList(collected1);
            final int n = 5000;
            Long[] array = generate(n);
            ArrayPublisher<Long> publisher = new ArrayPublisher<>(array);

            publisher.subscribe(new Subscriber<Long>() {

                private Subscription s;

                @Override
                public void onSubscribe(Subscription s) {
                    this.s = s;
                    for (int i = 0; i < n; i++) {
                        commonPool().execute(() -> s.request(1));
                    }
                }

                @Override
                public void onNext(Long aLong) {
                    collected.add(aLong);
                }

                @Override
                public void onError(Throwable t) {

                }

                @Override
                public void onComplete() {
                    latch.countDown();
                }
            });

            latch.await(2, SECONDS);

            final HashSet<Long> actual = new HashSet<>(collected);
            final HashSet<Long> expected = new HashSet<>(asList(array));
            System.out.println("actual " + actual.size());
            System.out.println("expected " + expected.size());
            System.out.println(Sets.difference(expected, actual));
            assertEquals(actual, expected);
        //}
    }

    @Test
    public void multithreadingTest2() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        ArrayList<Long> collected1 = new ArrayList<>();
        final List<Long> collected = synchronizedList(collected1);
        final int n = 5000;
        Long[] array = generate(n);
        ArrayPublisher<Long> optimizedPublisher = new ArrayPublisher<>(array);
        ConcurrentHashMap<String, List<Long>> receivedByThread = new ConcurrentHashMap<>();

        optimizedPublisher.subscribe(new Subscriber<Long>() {

            Subscription s;

            @Override
            public void onSubscribe(Subscription s) {
                this.s = s;
                commonPool().execute(() -> s.request(10));
            }

            @Override
            public void onNext(Long l) {
                commonPool().execute(() -> s.request(10));

                receivedByThread.compute(Thread.currentThread().getName(), (s, longs) -> {
                    if (longs == null) {
                        final ArrayList<Long> list = new ArrayList<>();
                        list.add(l);
                        return list;
                    } else {
                        longs.add(l);
                        return longs;
                    }
                });

                collected.add(l);
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        });

        latch.await(2, SECONDS);

        final HashSet<Long> actual = new HashSet<>(collected);
        final HashSet<Long> expected = new HashSet<>(asList(array));
        System.out.println("actual " + actual.size());
        System.out.println("expected " + expected.size());
        System.out.println(Sets.difference(expected, actual));
        assertEquals(actual, expected);
    }

    @Test
    public void multithreadingTest3() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        ArrayList<Long> collected1 = new ArrayList<>();
        final List<Long> collected = synchronizedList(collected1);
        final int n = 5000;
        Long[] array = generate(n);
        Flux<Long> optimizedPublisher = Flux.fromArray(array);
        ConcurrentHashMap<String, List<Long>> receivedByThread = new ConcurrentHashMap<>();

        optimizedPublisher.subscribe(new Subscriber<Long>() {

            Subscription s;

            @Override
            public void onSubscribe(Subscription s) {
                this.s = s;
                for (int i = 0; i < n; i++) {
                    commonPool().execute(() -> s.request(10));
                }
            }

            @Override
            public void onNext(Long l) {
                receivedByThread.compute(Thread.currentThread().getName(), (s, longs) -> {
                    if (longs == null) {
                        final ArrayList<Long> list = new ArrayList<>();
                        list.add(l);
                        return list;
                    } else {
                        longs.add(l);
                        return longs;
                    }
                });

                collected.add(l);
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        });

        latch.await(2, SECONDS);

        System.out.println(receivedByThread.keySet());

        final HashSet<Long> actual = new HashSet<>(collected);
        final HashSet<Long> expected = new HashSet<>(asList(array));
        System.out.println("actual " + actual.size());
        System.out.println("expected " + expected.size());
        System.out.println(Sets.difference(expected, actual));
        assertEquals(actual, expected);
    }

    static Long[] generate(long num) {
        return LongStream.range(0, num >= Integer.MAX_VALUE ? 1000000 : num)
                         .boxed()
                         .toArray(Long[]::new);
    }
}
