package org.test.reactive;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.Arrays;

public class ArrayPublisherPerfSample {

    public static void main(String[] args) {
        Integer[] array = new Integer[1000000];
        Arrays.fill(array, 777);
        ArrayPublisher<Integer> optimizedPublisher = new ArrayPublisher<>(array);

        while (true)
        optimizedPublisher.subscribe(new Subscriber<Integer>() {

            Subscription s;

            @Override
            public void onSubscribe(Subscription s) {
                this.s = s;
                s.request(1);
            }

            @Override
            public void onNext(Integer integer) {
                s.request(1);
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        });
    }
}
