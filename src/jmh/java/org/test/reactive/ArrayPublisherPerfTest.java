package org.test.reactive;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 2)
@Measurement(iterations = 5, time = 5)
@OutputTimeUnit(TimeUnit.SECONDS)
@Fork(value = 1)
@State(Scope.Thread)
public class ArrayPublisherPerfTest {

    @Param({ "1000000" })
    public int times;

    ArrayPublisher<Integer> threadSafePublisher;
    NonThreadsafeArrayPublisher<Integer> nonThreadsafeArrayPublisher;
    MyUnoptimizedArrayPublisher<Integer> myUnoptimizedArrayPublisher;
    UnoptimizedArrayPublisher<Integer> unoptimizedArrayPublisher;
    OptimizedPublisher<Integer> optimizedPublisher;

    @Setup
    public void setup() {
        Integer[] array = new Integer[times];
        Arrays.fill(array, 777);
        threadSafePublisher = new ArrayPublisher<>(array);
        nonThreadsafeArrayPublisher = new NonThreadsafeArrayPublisher<>(array);
        myUnoptimizedArrayPublisher = new MyUnoptimizedArrayPublisher<>(array);
        unoptimizedArrayPublisher = new UnoptimizedArrayPublisher<>(array);
        optimizedPublisher = new OptimizedPublisher<>(array);
    }

    @Benchmark
    public Object _0_myOptimizedPublisher(Blackhole bh) {
        PerfSubscriber lo = new PerfSubscriber(bh);

        threadSafePublisher.subscribe(lo);

        return lo;
    }

    @Benchmark
    public Object _1_myUnoptimizedPublisherPerformance(Blackhole bh) {
        PerfSubscriber lo = new PerfSubscriber(bh);

        myUnoptimizedArrayPublisher.subscribe(lo);

        return lo;
    }

    @Benchmark
    public Object _2_nonThreadSafePublisherPerformance(Blackhole bh) {
        PerfSubscriber lo = new PerfSubscriber(bh);

        nonThreadsafeArrayPublisher.subscribe(lo);

        return lo;
    }


    @Benchmark
    public Object _3_unoptimizedPublisherPerformance(Blackhole bh) {
        PerfSubscriber lo = new PerfSubscriber(bh);

        unoptimizedArrayPublisher.subscribe(lo);

        return lo;
    }

    @Benchmark
    public Object _4_optimizedPublisherPerformance(Blackhole bh) {
        PerfSubscriber lo = new PerfSubscriber(bh);

        optimizedPublisher.subscribe(lo);

        return lo;
    }
}
