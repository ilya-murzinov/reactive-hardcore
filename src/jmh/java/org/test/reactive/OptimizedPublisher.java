package org.test.reactive;

import com.google.common.collect.Sets;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;

import static java.util.Arrays.asList;
import static java.util.Collections.synchronizedList;
import static java.util.concurrent.ForkJoinPool.commonPool;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.test.reactive.ArrayPublisherTest.generate;
import static org.testng.Assert.assertEquals;

public class OptimizedPublisher<T> implements Publisher<T> {

    private final T[] array;

    public OptimizedPublisher(T[] array) {
        this.array = array;
    }

    @Override
    public void subscribe(Subscriber<? super T> subscriber) {
        subscriber.onSubscribe(new ArraySubscription<T>(array, subscriber));
    }

    private static class ArraySubscription<T> implements Subscription {

        final T[] array;
        final Subscriber<? super T> subscriber;

        int index;

        volatile long requested;
        static final AtomicLongFieldUpdater<ArraySubscription> REQUESTED =
                AtomicLongFieldUpdater.newUpdater(ArraySubscription.class, "requested");

        volatile boolean cancelled;

        public ArraySubscription(T[] array, Subscriber<? super T> subscriber) {
            this.array = array;
            this.subscriber = subscriber;
        }

        @Override
        public void request(long n) {
            if (n <= 0 && !cancelled) {
                cancel();
                subscriber.onError(new IllegalArgumentException(
                        "§3.9 violated: positive request amount required but it was " + n
                ));
                return;
            }

            long initialRequested;

            do {
                initialRequested = requested;

                if (initialRequested == Long.MAX_VALUE) {
                    return;
                }

                n = initialRequested + n;

                if (n <= 0) {
                    n = Long.MAX_VALUE;
                }

            } while (!REQUESTED.compareAndSet(this, initialRequested, n));

            if (initialRequested > 0) {
//                System.out.println(Thread.currentThread().getName() + " - RECURSION DETECTED");
                return;
            }

            if (n == Long.MAX_VALUE) {
                fastPath();
            }
            else {
                slowPath(n);
            }
        }

        void fastPath() {
            final Subscriber<? super T> s = subscriber;
            final T[] arr = array;
            int i = index;
            int length = arr.length;

            for (; i < length; i++) {
                if (cancelled) {
                    return;
                }

                T element = arr[i];

                if (element == null) {
                    s.onError(new NullPointerException());
                    return;
                }

                s.onNext(element);
            }

            if (cancelled) {
                return;
            }

            s.onComplete();
        }

        void slowPath(long n) {
            final Subscriber<? super T> s = subscriber;
            final T[] arr = array;
            int sent = 0;
            int i = index;
            int length = arr.length;

            while (true) {
                for (; sent < n && i < length; sent++, i++) {
                    if (cancelled) {
                        return;
                    }

                    T element = arr[i];

                    if (element == null) {
                        s.onError(new NullPointerException());
                        return;
                    }

                    s.onNext(element);
                }

                if (cancelled) {
                    return;
                }

                if (i == length) {
                    s.onComplete();
                    return;
                }

                n = requested;
                if (n == sent) {
                    index = i;
                    if (REQUESTED.addAndGet(this, -sent) == 0) {
                        return;
                    }
                    sent = 0;
                }
            }
        }

        @Override
        public void cancel() {
            cancelled = true;
        }
    }

    public static void main(String[] args) throws Exception {
        CountDownLatch latch = new CountDownLatch(1);
        ArrayList<Long> collected1 = new ArrayList<>();
        final List<Long> collected = synchronizedList(collected1);
        final int n = 5000;
        Long[] array = generate(n);
        OptimizedPublisher<Long> optimizedPublisher = new OptimizedPublisher<>(array);
        ConcurrentHashMap<String, List<Long>> receivedByThread = new ConcurrentHashMap<>();

        optimizedPublisher.subscribe(new Subscriber<Long>() {

            Subscription s;

            @Override
            public void onSubscribe(Subscription s) {
                this.s = s;
                for (int i = 0; i < n; i++) {
                    commonPool().execute(() -> s.request(10));
                }
            }

            @Override
            public void onNext(Long l) {
                receivedByThread.compute(Thread.currentThread().getName(), (s, longs) -> {
                    if (longs == null) {
                        final ArrayList<Long> list = new ArrayList<>();
                        list.add(l);
                        return list;
                    } else {
                        longs.add(l);
                        return longs;
                    }
                });

                collected.add(l);
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        });

        latch.await(2, SECONDS);

        System.out.println(receivedByThread.keySet());

        final HashSet<Long> actual = new HashSet<>(collected);
        final HashSet<Long> expected = new HashSet<>(asList(array));
        System.out.println("actual " + actual.size());
        System.out.println("expected " + expected.size());
        System.out.println(Sets.difference(expected, actual));
        assertEquals(actual, expected);
    }
}
